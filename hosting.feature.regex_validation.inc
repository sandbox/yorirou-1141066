<?php

function regex_validation_hosting_feature() {
  $features = array();

  $features['regex_validation'] = array(
    'title' => t('Regex validation of the urls'),
    'description' => t(''),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'regex_validation',
    'group' => 'experimental',
  );

  return $features;
}
